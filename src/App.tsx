import React from 'react';
import { Routes, Route } from 'react-router-dom';
import './App.css';
import CoinsList from './views/CoinsList';
import Coin from './views/Coin';

function App() {
  return (
    <div className="App">
      <Routes>
        <Route path="/" element={<CoinsList />} />
        <Route path="/coin/:name" element={<Coin />} />
      </Routes>
    </div>
  );
}

export default App;
