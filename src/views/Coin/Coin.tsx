import React, { useEffect, useState } from 'react';
import { Chart } from "react-google-charts";
import {
  Link,
  useParams
} from "react-router-dom";
import { CRYPTO_COMPARE_API_URL, CRYPTO_COMPARE_URL, CRYPTO_COMPARE_API_KEY } from '../../lib/constants'
import './Coin.css';

const DEFAULT_DAYS_NUMBER = 7;
const EXPANDED_DAYS_NUMBER = 30;

interface CoinData {
  fullName: string,
  imageUrl: string,
  price: number,
  creationDate: string,
  blockHeight: number
}

function Coin() {
  const [coinData, setCoinData] = useState<CoinData>({
    fullName: '',
    imageUrl: '',
    price: 0,
    creationDate: '',
    blockHeight: 0
  });
  const [dailyData, setDailyData] = useState<Array<{ high: number, low: number, open: number, close: number, time: number }>>([])
  const [daysNumber, setDaysNumber] = useState<number>(DEFAULT_DAYS_NUMBER)
  const { name } = useParams();

  useEffect(() => {
    fetch(`${CRYPTO_COMPARE_API_URL}/data/blockchain/mining/calculator?fsyms=${name}&tsyms=USD&api_key=${CRYPTO_COMPARE_API_KEY}`)
    .then(response => {
      if(!response.ok) {
        throw response;
      }

      return response.json();
    })
    .then(({ Data: data }) => {
      const { CoinInfo: { FullName, ImageUrl, AssetLaunchDate, BlockNumber }, Price: { USD } } = data[name || ''];

      setCoinData({
        ...coinData,
        fullName: FullName,
        imageUrl: `${CRYPTO_COMPARE_URL}${ImageUrl}`,
        price: USD,
        creationDate: AssetLaunchDate,
        blockHeight: BlockNumber
      });
    }).catch(error => {
      console.log(error);
    })

    fetch(`${CRYPTO_COMPARE_API_URL}/data/v2/histoday?fsym=${name}&tsym=USD&limit=30&api_key=${CRYPTO_COMPARE_API_KEY}`)
    .then(response => {
      if(!response.ok) {
        throw response;
      }

      return response.json();
    })
    .then(({ Data: { Data: dailyData } }) => {
      setDailyData(dailyData);
    }).catch(error => {
      console.log(error);
    })
  }, []);

  const { fullName, imageUrl, price, creationDate, blockHeight } = coinData;
  const dailyDataByDaysNumber = dailyData.slice(dailyData.length - daysNumber, dailyData.length);
  const nonSelectedDaysNumber = daysNumber === DEFAULT_DAYS_NUMBER ? EXPANDED_DAYS_NUMBER : DEFAULT_DAYS_NUMBER;
  
  return (
    <div className="Coin">
      <div>
        <Link to={'/'}>Back</Link>
      </div>
      <div>
        <img className="icon" src={imageUrl} alt={fullName} />
      </div>
      <div className="name">
        {fullName}
      </div>
      <div>Price: &#36;{price}</div>
      <div>Date of creation: {creationDate}</div>
      <div>Number of blocks: {blockHeight}</div>
      <div>
        <Chart
          width={'100%'}
          height={350}
          chartType="CandlestickChart"
          data={[
            ['Day', 'Open', 'Low', 'Close', 'High'],
            ...dailyDataByDaysNumber.map(({ high, low, open, close, time }) => [getDate(time), low, open, close, high])
          ]}
          options={{
            title: `Last ${daysNumber} days`,
            legend: 'none',
            enableInteractivity: false,
            candlestick: {
              fallingColor: { strokeWidth: 0, fill: '#a52714' },
              risingColor: { strokeWidth: 0, fill: '#0f9d58' }
            }
          }}
        />
      </div>
      <div>
        <button onClick={toggleDaysNumber}>Show last {nonSelectedDaysNumber} days</button>
      </div>
    </div>
  );

  function getDate(timestampInSeconds: number) {
      const milisecondsInSecond = 1000;

    return new Date(timestampInSeconds * milisecondsInSecond);
  }

  function toggleDaysNumber() {
    setDaysNumber(nonSelectedDaysNumber);
  }
}

export default Coin;
