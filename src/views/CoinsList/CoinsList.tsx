import React, { useEffect, useState } from 'react';
import {
  Link
} from "react-router-dom";
import { CRYPTO_COMPARE_API_URL, CRYPTO_COMPARE_URL, CRYPTO_COMPARE_API_KEY } from '../../lib/constants'
import './CoinsList.css';

interface CoinsListItemData {
  imageUrl: string,
  name: string,
  price: number,
  formattedPrice: string,
  percentagesDailyChange: number
}

const COINS_SYMBOLS = {
  BTC: 'BTC',
  ETH: 'ETH',
  ADA: 'ADA',
  SOL: 'SOL',
  DOT: 'DOT'
};
const CURRENCY = 'USD';
const SORT_OPTIONS = {
  NAME: 'NAME',
  PRICE: 'PRICE'
};

function CoinsList() {
  const [coinsData, setCoinsData] = useState<object>({});
  const [pinnedCoin, setPinnedCoin] = useState<string>('');
  const [sortProperty, setSortProperty] = useState<string>('');

  useEffect(() => {
    const coinsSymbols = Object.values(COINS_SYMBOLS).join(',');

    fetch(`${CRYPTO_COMPARE_API_URL}/data/pricemultifull?fsyms=${coinsSymbols}&tsyms=USD&api_key=${CRYPTO_COMPARE_API_KEY}`)
    .then(response => {
      if(!response.ok) {
        throw response;
      }

      return response.json();
    })
    .then(data => {
      const coins = Object.values(COINS_SYMBOLS).reduce(toCoin, {});

      setCoinsData(coins);

      function toCoin(coinsDataBySymbol: object, coinSymbol: string) {
        const {
          DISPLAY: { [coinSymbol]: { [CURRENCY]: { PRICE: FORMATTED_PRICE } } },
          RAW: { [coinSymbol]: { [CURRENCY]: { FROMSYMBOL, IMAGEURL, PRICE, CHANGEPCTDAY } } }
        } = data;
        const coinData = {
          imageUrl: `${CRYPTO_COMPARE_URL}${IMAGEURL}`,
          name: FROMSYMBOL,
          price: PRICE,
          formattedPrice: FORMATTED_PRICE,
          percentagesDailyChange: CHANGEPCTDAY
        };
    
        return { ...coinsDataBySymbol, [coinSymbol]: coinData };
      }
    }).catch(error => {
      console.log(error);
    })
  }, []);

  const coins: Array<CoinsListItemData> = Object.values(coinsData).sort(maybeSortCoins);
  const sortedClassName = 'sorted-by-property';
  const isSortedByName = sortProperty === SORT_OPTIONS.NAME;
  const isSortedByPrice = sortProperty === SORT_OPTIONS.PRICE;

  return (
    <div className="CoinsList">
      <table className="coins-list-table">
        <thead>
          <tr>
            <th />
            <th>Symbol</th>
            <th>Name <button className={isSortedByName ? sortedClassName : ''} onClick={sortBy(SORT_OPTIONS.NAME)}>V</button></th>
            <th>Price <button className={isSortedByPrice ? sortedClassName : ''} onClick={sortBy(SORT_OPTIONS.PRICE)}>V</button></th>
            <th>Daily change</th>
          </tr>
        </thead>
        <tbody>
          {coins.map((coinProps, index) => (<tr className="coins-list-item" key={index}><CoinsListItem {...coinProps} /></tr>))}
        </tbody>
      </table>
    </div>
  );

  function pinCoin(coinSymbol: string) {
    return () => {
      setPinnedCoin(pinnedCoin !== coinSymbol ? coinSymbol : '');
    };
  }

  function sortBy(sortOption: string) {
    return () => {
      setSortProperty(sortProperty !== sortOption ? sortOption : '');
    }
  }

  function maybeSortCoins(coinA: CoinsListItemData, coinB: CoinsListItemData) {
    if(coinB.name === pinnedCoin) {
      return 1;
    }

    if(coinA.name === pinnedCoin) {
      return -1;
    }

    if(sortProperty === SORT_OPTIONS.NAME) {
      if (coinA.name < coinB.name) {
        return -1;
      }

      if (coinA.name > coinB.name) {
        return 1;
      }
    
      return 0;
    }

    if(sortProperty === SORT_OPTIONS.PRICE) {
      return coinA.price - coinB.price
    }

    return 0;
  }

  function CoinsListItem({ imageUrl, name, formattedPrice, percentagesDailyChange }: CoinsListItemData) {
    const isPinned = name === pinnedCoin;
    const maybePinnedCoinClassName = isPinned ? 'is-pinned' : '';
    const dailyChangeClassName = `${percentagesDailyChange >= 0 ? 'positive' : 'negative'}-daily-change`;
  
    return (
      <>
        <td className="pin-button-wrapper">
          <button className={`pin-button ${maybePinnedCoinClassName}`} onClick={pinCoin(name)}>{isPinned ? 'Pinned' : 'Pin'}</button>
        </td>
        <td><img className="icon" src={imageUrl} alt={name} /></td>
        <td><Link to={`/coin/${name}`}>{name}</Link></td>
        <td>{formattedPrice}</td>
        <td className={dailyChangeClassName}>{percentagesDailyChange.toFixed(2)}&#37;</td>
      </>
    );
  }
}

export default CoinsList;
